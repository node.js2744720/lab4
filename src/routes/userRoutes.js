const express = require('express');
const router = express.Router();
const User = require('../models/user');
const auth = require('../middleware/auth');

router.get('/users/me', auth, async (req, res) => {
    res.send(req.user);
});

router.post('/users/register', async (req, res) => {
    const userData = req.body;
    try {
        const newUser = new User(userData);
        await newUser.save();
        const token = await newUser.generateAuthToken();
        res.status(201).send({ user: newUser, token });
    } catch (error) {
        res.status(400).send(error.message);
    }
});

router.post("/users/login", async (req, res) => {
    try {
        const user = await User.findByCredentials(req.body.email, req.body.password);
        const token = await user.generateAuthToken();
        res.send({ user, token });
    } catch (e) {
        res.status(400).send(e.message);
    }
});

router.post('/users/logout', auth, async (req, res) => {
    try {
        req.user.tokens = req.user.tokens.filter((token) => token.token !== req.token);
        await req.user.save();
        res.send('Successfully logged out');
    } catch (error) {
        res.status(500).send('Internal Server Error');
    }
});

router.post('/users/logoutAll', auth, async (req, res) => {
    try {
        req.user.tokens = [];
        await req.user.save();
        res.send('Successfully logged out from all devices');
    } catch (error) {
        res.status(500).send('Internal Server Error');
    }
});


router.get("/users", (req, res) => {
    // Знайти всіх користувачів у базі даних
    User.find({})
        .then((users) => {
            // Відправити користувачів у відповідь як список
            res.status(200).send(users);
        })
        .catch((error) => {
            // Відправити статус помилки 500, якщо виникла помилка при обробці запиту
            res.status(500).send('Internal Server Error');
        });
});

router.get('/users/:id', (req, res) => {
    const userId = req.params.id;
    // Знайти користувача за його id у базі даних
    User.findById(userId)
        .then(user => {
            if (!user) {
                // Якщо користувача не знайдено, відправити повідомлення про помилку
                return res.status(404).json({ message: 'User not found' });
            }
            // Відправити знайденого користувача у відповідь
            res.status(200).json(user);
        })
        .catch(error => {
            // Відправити статус помилки 500, якщо виникла помилка при обробці запиту
            console.error(error);
            res.status(500).send('Internal Server Error');
        });
});


router.delete('/user/:id', async (req, res) => {
    const userId = req.params.id;

    try {
        // Знайти та видалити користувача за його id у базі даних
        const user = await User.findByIdAndDelete(userId);

        if (!user) {
            // Якщо користувача не знайдено, відправити повідомлення про помилку
            return res.status(404).send("User not found");
        }

        // Відправити підтвердження про успішне видалення користувача
        res.status(200).send("User deleted successfully");
    } catch (error) {
        // Відправити статус помилки 400, якщо виникла помилка при видаленні користувача
        console.error(error);
        res.status(400).send("Error deleting user");
    }
});

router.delete('/users', async (req, res) => {
    try {
        // Видалити всіх користувачів з бази даних
        await User.deleteMany({});
        // Відправити підтвердження про успішне видалення всіх користувачів
        res.status(200).send("All users deleted successfully");
    } catch (error) {
        // Відправити статус помилки 400, якщо виникла помилка при видаленні користувачів
        console.error(error);
        res.status(400).send("Error deleting users");
    }
});


router.get('/test', (req, res) => {
    res.send("From a new File");
});

router.patch("/users/:id", async (req, res) => {
    try {
        const userId = req.params.id;
        const updates = req.body;

        const user = await User.findById(userId);

        if (!user) {
            return res.status(404).send("User not found");
        }

        Object.keys(updates).forEach((field) => {
            user[field] = updates[field];
        });

        await user.save();

        res.status(200).send(user);
    } catch (error) {
        console.error(error);
        res.status(500).send("Internal Server Error");
    }
});

module.exports = router;