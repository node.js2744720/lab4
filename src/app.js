const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');


// Імпорт моделі користувача з файлу user.js
const User = require('./models/user');

// Імпорт моделі завдання з файлу task.js
const Task = require('./models/task.js');

// Імпорт файлу з маршрутами користувачів
const userRoutes = require('./routes/userRoutes');

// Імпорт файлу з маршрутами користувачів
const taskRoutes = require('./routes/taskRoutes');

// Створення екземпляра додатку Express
const app = express();

// Завантаження змінних середовища з файлу .env
dotenv.config();

// Додавання маршрутів
app.use(express.json());

// Додавання маршрутів
app.use(userRoutes);

app.use(taskRoutes);

// Підключення до бази даних MongoDB
mongoose.connect(process.env.MONGO_URL)
    .then(() => {
        console.log("Connected to MongoDB");
    })
    .catch((error) => {
        console.error("Error connecting to MongoDB:", error);
    });

// Прослуховування порту
app.listen(3000, () => {
    console.log('Server is running on port 3000');
});